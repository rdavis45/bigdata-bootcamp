import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier

import utils

# PLEASE USE THE GIVEN FUNCTION NAME, DO NOT CHANGE IT
# USE THIS RANDOM STATE FOR ALL OF THE PREDICTIVE MODELS
# THE TESTS WILL NEVER PASS
RANDOM_STATE = 545510477


# input: X_train, Y_train and X_test
# output: Y_pred
def logistic_regression_pred(X_train, Y_train, X_test):
    logReg = LogisticRegression(random_state=RANDOM_STATE)
    logReg.fit(X_train, Y_train)
    pred = logReg.predict(X_test)
    # train a logistic regression classifier using X_train and Y_train. Use this to predict labels of X_train
    # use default params for the classifier
    return pred  # input: X_train, Y_train


# input: X_train, Y_train and X_test
# output: Y_pred
def svm_pred(X_train, Y_train, X_test):
    # TODO:train a SVM classifier using X_train and Y_train. Use this to predict labels of X_test
    # use default params for the classifier
    clf_svm = LinearSVC(random_state=RANDOM_STATE)
    clf_svm.fit(X_train, Y_train)
    pred = clf_svm.predict(X_test)
    return pred


# input: X_train, Y_train and X_test
# output: Y_pred
def decisionTree_pred(X_train, Y_train, X_test):
    # TODO:train a logistic regression classifier using X_train and Y_train. Use this to predict labels of X_test
    # IMPORTANT: use max_depth as 5. Else your test cases might fail.
    clf_tree = DecisionTreeClassifier(max_depth=5, random_state=RANDOM_STATE)
    clf_tree.fit(X_train, Y_train)
    pred = clf_tree.predict(X_test)
    return pred


# input: Y_pred,Y_true
# output: accuracy, auc, precision, recall, f1-score
def classification_metrics(Y_pred, Y_true):
    # TODO: Calculate the above mentioned metrics
    # NOTE: It is important to provide the output in the same order
    accuracy = metrics.accuracy_score(y_true=Y_true, y_pred=Y_pred)
    fpr, tpr, thresholds = metrics.roc_curve(y_true=Y_true, y_score=Y_pred)
    auc = metrics.auc(fpr, tpr)
    precision = metrics.precision_score(y_true=Y_true, y_pred=Y_pred)
    recall = metrics.recall_score(y_true=Y_true, y_pred=Y_pred)
    f1score = metrics.f1_score(y_true=Y_true, y_pred=Y_pred)
    return accuracy, auc, precision, recall, f1score


# input: Name of classifier, predicted labels, actual labels
def display_metrics(classifierName, Y_pred, Y_true):
    print "______________________________________________"
    print "Classifier: " + classifierName
    acc, auc_, precision, recall, f1score = classification_metrics(Y_pred, Y_true)
    print "Accuracy: " + str(acc)
    print "AUC: " + str(auc_)
    print "Precision: " + str(precision)
    print "Recall: " + str(recall)
    print "F1-score: " + str(f1score)
    print "______________________________________________"
    print ""


def main():
    X_train, Y_train = utils.get_data_from_svmlight("../deliverables/features_svmlight.train")
    X_test, Y_test = utils.get_data_from_svmlight("../data/features_svmlight.validate")

    display_metrics("Logistic Regression", logistic_regression_pred(X_train, Y_train, X_test), Y_test)
    display_metrics("SVM", svm_pred(X_train, Y_train, X_test), Y_test)
    display_metrics("Decision Tree", decisionTree_pred(X_train, Y_train, X_test), Y_test)


if __name__ == "__main__":
    main()
