import time

import pandas as pd
from datetime import datetime


# PLEASE USE THE GIVEN FUNCTION NAME, DO NOT CHANGE IT

def read_csv(filepath):
    '''
    TODO : This function needs to be completed.
    Read the events.csv and mortality_events.csv files. 
    Variables returned from this function are passed as input to the metric functions.
    '''
    dateparse = lambda x: pd.datetime.strptime(x, '%Y-%m-%d')

    events = pd.read_csv(filepath + 'events.csv', date_parser=dateparse, parse_dates=["timestamp"])
    mortality = pd.read_csv(filepath + 'mortality_events.csv', date_parser=dateparse, parse_dates=["timestamp"])

    return events, mortality


def event_count_metrics(events, mortality):
    '''
    TODO : Implement this function to return the event count metrics.
    Event count is defined as the number of events recorded for a given patient.
    '''

    event_counts = events\
        .join(mortality.set_index("patient_id"), how="outer", on="patient_id", rsuffix="_death")

    event_counts = event_counts[event_counts["timestamp_death"].isnull()]\
        .groupby("patient_id")\
        .size()

    death_counts = events\
        .join(mortality.set_index("patient_id"), how="inner", on="patient_id", rsuffix="_death")\
        .groupby("patient_id")\
        .size()

    avg_dead_event_count = death_counts.values.mean()
    max_dead_event_count = death_counts.values.max()
    min_dead_event_count = death_counts.values.min()
    avg_alive_event_count = event_counts.values.mean()
    max_alive_event_count = event_counts.values.max()
    min_alive_event_count = event_counts.values.min()

    return min_dead_event_count, max_dead_event_count, avg_dead_event_count, min_alive_event_count, max_alive_event_count, avg_alive_event_count


def encounter_count_metrics(events, mortality):
    '''
    TODO : Implement this function to return the encounter count metrics.
    Encounter count is defined as the count of unique dates on which a given patient visited the ICU. 
    '''

    event_counts = events \
        .join(mortality.set_index("patient_id"), how="outer", on="patient_id", rsuffix="_death")

    event_counts = event_counts[event_counts["timestamp_death"].isnull()] \
        .loc[:, ["patient_id", "timestamp"]] \
        .drop_duplicates() \
        .groupby("patient_id") \
        .size()

    death_counts = events \
        .join(mortality.set_index("patient_id"), how="inner", on="patient_id", rsuffix="_death") \
        .loc[:, ["patient_id", "timestamp"]] \
        .drop_duplicates() \
        .groupby("patient_id") \
        .size()


    avg_dead_encounter_count = death_counts.values.mean()
    max_dead_encounter_count = death_counts.values.max()
    min_dead_encounter_count = death_counts.values.min()
    avg_alive_encounter_count = event_counts.values.mean()
    max_alive_encounter_count = event_counts.values.max()
    min_alive_encounter_count = event_counts.values.min()

    return min_dead_encounter_count, max_dead_encounter_count, avg_dead_encounter_count, min_alive_encounter_count, max_alive_encounter_count, avg_alive_encounter_count


def record_length_metrics(events, mortality):
    '''
    TODO: Implement this function to return the record length metrics.
    Record length is the duration between the first event and the last event for a given patient. 
    '''

    event_counts = events \
        .join(mortality.set_index("patient_id"), how="outer", on="patient_id", rsuffix="_death")

    event_counts = event_counts[event_counts["timestamp_death"].isnull()] \
        .loc[:, ["patient_id", "timestamp"]] \
        .groupby("patient_id") \
        .apply(calc_length)

    death_counts = events \
                       .join(mortality.set_index("patient_id"), how="inner", on="patient_id", rsuffix="_death") \
                       .groupby("patient_id").apply(calc_length)


    avg_dead_rec_len = death_counts.values.mean()
    max_dead_rec_len = death_counts.values.max()
    min_dead_rec_len = death_counts.values.min()
    avg_alive_rec_len = event_counts.values.mean()
    max_alive_rec_len = event_counts.values.max()
    min_alive_rec_len = event_counts.values.min()

    return min_dead_rec_len, max_dead_rec_len, avg_dead_rec_len, min_alive_rec_len, max_alive_rec_len, avg_alive_rec_len

def calc_length(values):

    last = values['timestamp'].max()
    first = values['timestamp'].min()

    return int((last-first).days)

def main():
    '''
    You may change the train_path variable to point to your train data directory.
    OTHER THAN THAT, DO NOT MODIFY THIS FUNCTION.
    '''
    # You may change the following line to point the train_path variable to your train data directory
    train_path = '../data/train/'

    # DO NOT CHANGE ANYTHING BELOW THIS ----------------------------
    events, mortality = read_csv(train_path)

    # Compute the event count metrics
    start_time = time.time()
    event_count = event_count_metrics(events, mortality)
    end_time = time.time()
    print("Time to compute event count metrics: " + str(end_time - start_time) + "s")
    print event_count

    # Compute the encounter count metrics
    start_time = time.time()
    encounter_count = encounter_count_metrics(events, mortality)
    end_time = time.time()
    print("Time to compute encounter count metrics: " + str(end_time - start_time) + "s")
    print encounter_count

    # Compute record length metrics
    start_time = time.time()
    record_length = record_length_metrics(events, mortality)
    end_time = time.time()
    print("Time to compute record length metrics: " + str(end_time - start_time) + "s")
    print record_length


if __name__ == "__main__":
    main()
