from datetime import timedelta
from operator import itemgetter

import pandas as pd

import utils


# PLEASE USE THE GIVEN FUNCTION NAME, DO NOT CHANGE IT

def read_csv(filepath):
    '''
    TODO: This function needs to be completed.
    Read the events.csv, mortality_events.csv and event_feature_map.csv files into events, mortality and feature_map.
    
    Return events, mortality and feature_map
    '''
    dateparse = lambda x: pd.datetime.strptime(x, '%Y-%m-%d')

    # Columns in events.csv - patient_id,event_id,event_description,timestamp,value
    events = pd.read_csv(filepath + 'events.csv', date_parser=dateparse, parse_dates=["timestamp"])

    # Columns in mortality_event.csv - patient_id,timestamp,label
    mortality = pd.read_csv(filepath + 'mortality_events.csv', date_parser=dateparse, parse_dates=["timestamp"])

    # Columns in event_feature_map.csv - idx,event_id
    feature_map = pd.read_csv(filepath + 'event_feature_map.csv')

    return events, mortality, feature_map


def calculate_index_date(events, mortality, deliverables_path):
    '''
    TODO: This function needs to be completed.

    Refer to instructions in Q3 a

    Suggested steps:
    1. Create list of patients alive ( mortality_events.csv only contains information about patients deceased)
    2. Split events into two groups based on whether the patient is alive or deceased
    3. Calculate index date for each patient
    
    IMPORTANT:
    Save indx_date to a csv file in the deliverables folder named as etl_index_dates.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, indx_date.
    The csv file should have a header 
    For example if you are using Pandas, you could write: 
        indx_date.to_csv(deliverables_path + 'etl_index_dates.csv', columns=['patient_id', 'indx_date'], index=False)

    Return indx_date
    '''

    alive = events \
        .join(mortality.set_index("patient_id"), how="outer", on="patient_id", rsuffix="_death")

    alive = alive[alive["timestamp_death"].isnull()] \
                .loc[:, ["patient_id", "timestamp"]] \
        .groupby("patient_id") \
        .apply(get_alive_index_date)

    dead = events \
        .join(mortality.set_index("patient_id"), how="inner", on="patient_id", rsuffix="_death")

    dead = dead.loc[:, ["patient_id", "timestamp_death"]].drop_duplicates()

    # dead.columns = ['patient_id', 'timestamp']

    dead["indx_date"] = dead["timestamp_death"].apply(get_dead_index_date)

    dead = dead.loc[:, ["patient_id", "indx_date"]]

    if not alive.empty:
        alive = alive.to_frame().reset_index()
        alive.columns = ["patient_id", "indx_date"]
    indx_date = pd.concat([alive, dead])

    indx_date.to_csv(deliverables_path + 'etl_index_dates.csv', columns=['patient_id', 'indx_date'], index=False)
    return indx_date


def get_dead_index_date(value):
    return value - timedelta(days=30)


def get_alive_index_date(values):
    return values['timestamp'].max()


def filter_events(events, indx_date, deliverables_path):
    '''
    TODO: This function needs to be completed.

    Refer to instructions in Q3 b

    Suggested steps:
    1. Join indx_date with events on patient_id
    2. Filter events occuring in the observation window(IndexDate-2000 to IndexDate)
    
    
    IMPORTANT:
    Save filtered_events to a csv file in the deliverables folder named as etl_filtered_events.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, event_id, value.
    The csv file should have a header 
    For example if you are using Pandas, you could write: 
        filtered_events.to_csv(deliverables_path + 'etl_filtered_events.csv', columns=['patient_id', 'event_id', 'value'], index=False)

    Return filtered_events
    '''

    joined_events = events.join(indx_date.set_index("patient_id"), how="inner", on="patient_id")
    filtered_events = joined_events[
        (joined_events["indx_date"] - joined_events["timestamp"]).apply(lambda x: 0 <= int(x.days) <= 2000)]
    filtered_events = filtered_events.loc[:, ['patient_id', 'event_id', 'value']]
    filtered_events.to_csv(deliverables_path + 'etl_filtered_events.csv', columns=['patient_id', 'event_id', 'value'],
                           index=False)
    return filtered_events


def aggregate_events(filtered_events_df, mortality_df, feature_map_df, deliverables_path):
    '''
    TODO: This function needs to be completed.

    Refer to instructions in Q3 c

    Suggested steps:
    1. Replace event_id's with index available in event_feature_map.csv
    2. Remove events with n/a values
    3. Aggregate events using sum and count to calculate feature value
    4. Normalize the values obtained above using min-max normalization(the min value will be 0 in all scenarios)
    
    
    IMPORTANT:
    Save aggregated_events to a csv file in the deliverables folder named as etl_aggregated_events.csv. 
    Use the global variable deliverables_path while specifying the filepath. 
    Each row is of the form patient_id, event_id, value.
    The csv file should have a header .
    For example if you are using Pandas, you could write: 
        aggregated_events.to_csv(deliverables_path + 'etl_aggregated_events.csv', columns=['patient_id', 'feature_id', 'feature_value'], index=False)

    Return filtered_events
    '''
    filtered_events_df = filtered_events_df.dropna()
    filtered_events_df = filtered_events_df.join(feature_map_df.set_index("event_id"), on="event_id").reset_index()
    filtered_events_df = filtered_events_df.dropna().loc[:, ["patient_id", "event_id", "value", "idx"]]
    grouped_events = filtered_events_df.groupby(["patient_id", "event_id"])
    aggregated_events = grouped_events.apply(calc_event_info).drop_duplicates()
    aggregated_events.columns = ["patient_id", "event_id", "feature_value", "feature_id"]
    aggregated_events = aggregated_events.loc[:, ["patient_id", "feature_id", "feature_value"]]
    aggregated_events = aggregated_events.groupby("feature_id").apply(normalize_event_info)

    aggregated_events.to_csv(deliverables_path + 'etl_aggregated_events.csv',
                             columns=['patient_id', 'feature_id', 'feature_value'], index=False)
    return aggregated_events


def calc_event_info(values):
    if "LAB" in values["event_id"].values[0]:
        out = values["value"].count()
    elif "DIAG" in values["event_id"].values[0]:
        out = values["value"].values.sum()
    elif "DRUG" in values["event_id"].values[0]:
        out = values["value"].values.sum()
    else:
        out = None

    values["value"] = out
    return values


def normalize_event_info(values):
    feature_value = values["feature_value"].values
    max_val = float(feature_value.max())
    normalized_vals = (feature_value) / (max_val)
    values["feature_value"] = normalized_vals
    return values


def create_features(events, mortality, feature_map):
    deliverables_path = '../deliverables/'

    # Calculate index date
    indx_date = calculate_index_date(events, mortality, deliverables_path)

    # Filter events in the observation window
    filtered_events = filter_events(events, indx_date, deliverables_path)

    # Aggregate the event values for each patient
    aggregated_events = aggregate_events(filtered_events, mortality, feature_map, deliverables_path)

    complete_events = aggregated_events.join(mortality.set_index("patient_id"), on="patient_id", how="outer")

    complete_events = complete_events.loc[:,["patient_id", "feature_id", "feature_value", "label"]]\
        .dropna(subset=["feature_id", "feature_value"]).fillna(0.0)

    '''
    TODO: Complete the code below by creating two dictionaries - 
    1. patient_features :  Key - patient_id and value is array of tuples(feature_id, feature_value)
    2. mortality : Key - patient_id and value is mortality label
    '''
    patient_features = {}
    mortality = {}

    for index, row in complete_events.iterrows():
        patient_id = row["patient_id"]
        feature_id = row["feature_id"]
        if feature_id == 0:
            print patient_id, feature_id
        feature_value = row["feature_value"]
        if patient_id not in patient_features.keys():
            patient_features[patient_id] = [(feature_id, feature_value)]
        else:
            patient_features[patient_id].append((feature_id, feature_value))

        mortality[patient_id] = row['label']

    return patient_features, mortality


def save_svmlight(patient_features, mortality, op_file, op_deliverable):
    '''
    TODO: This function needs to be completed

    Refer to instructions in Q3 d

    Create two files:
    1. op_file - which saves the features in svmlight format. (See instructions in Q3d for detailed explanation)
    2. op_deliverable - which saves the features in following format:
       patient_id1 label feature_id:feature_value feature_id:feature_value feature_id:feature_value ...
       patient_id2 label feature_id:feature_value feature_id:feature_value feature_id:feature_value ...  
    
    Note: Please make sure the features are ordered in ascending order, and patients are stored in ascending order as well.     
    '''
    deliverable1 = open(op_file, 'wb')
    deliverable2 = open(op_deliverable, 'wb')

    patient_ids = patient_features.keys()
    patient_ids.sort()
    for id in patient_ids:
        features = patient_features[id]
        features = sorted(features, key=itemgetter(0))
        label = mortality[id]
        deliverable1.write("{0} {1} \n".format(int(label), utils.bag_to_svmlight(features)))
        deliverable2.write("{0} {1} {2} \n".format(int(id), int(label), utils.bag_to_svmlight(features)))


def main():
    train_path = '../data/train/'
    events, mortality, feature_map = read_csv(train_path)
    patient_features, mortality = create_features(events, mortality, feature_map)
    save_svmlight(patient_features, mortality, '../deliverables/features_svmlight.train',
                  '../deliverables/features.train')


if __name__ == "__main__":
    main()
