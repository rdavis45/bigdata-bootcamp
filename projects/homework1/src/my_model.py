from operator import itemgetter

import numpy as np
import pandas as pd
from numpy import mean
from sklearn import metrics
from sklearn.cross_validation import KFold
from sklearn.ensemble import GradientBoostingClassifier

import utils

# Note: You can reuse code that you wrote in etl.py and models.py and cross.py over here. It might help.
# PLEASE USE THE GIVEN FUNCTION NAME, DO NOT CHANGE IT

'''
You may generate your own features over here.
Note that for the test data, all events are already filtered such that they fall in the observation window of their respective patients. Thus, if you were to generate features similar to those you constructed in code/etl.py for the test data, all you have to do is aggregate events for each patient.
IMPORTANT: Store your test data features in a file called "test_features.txt" where each line has the
patient_id followed by a space and the corresponding feature in sparse format.
Eg of a line:
60 971:1.000000 988:1.000000 1648:1.000000 1717:1.000000 2798:0.364078 3005:0.367953 3049:0.013514
Here, 60 is the patient id and 971:1.000000 988:1.000000 1648:1.000000 1717:1.000000 2798:0.364078 3005:0.367953 3049:0.013514 is the feature for the patient with id 60.

Save the file as "test_features.txt" and save it inside the folder deliverables

input:
output: X_train,Y_train,X_test
'''


def read_test_csv(filepath):
    # Columns in events.csv - patient_id,event_id,event_description,timestamp,value
    events_df = pd.read_csv(filepath + 'events.csv', parse_dates=['timestamp'])

    # Columns in event_feature_map.csv - idx,event_id
    feature_map_df = pd.read_csv(filepath + 'event_feature_map.csv')

    return events_df, feature_map_df


def build_features(events_df, feature_map_df):
    events_df = events_df.dropna()
    events_df = events_df.join(feature_map_df.set_index("event_id"), on="event_id").reset_index()
    events_df = events_df.dropna().loc[:, ["patient_id", "event_id", "value", "idx"]]
    grouped_events = events_df.groupby(["patient_id", "event_id"])
    aggregated_events = grouped_events.apply(calc_event_info).drop_duplicates()
    aggregated_events.columns = ["patient_id", "event_id", "feature_value", "feature_id"]
    aggregated_events = aggregated_events.loc[:, ["patient_id", "feature_id", "feature_value"]]
    aggregated_events = aggregated_events.groupby("feature_id").apply(normalize_event_info)

    # aggregated_events.to_csv(deliverables_path + 'etl_aggregated_events.csv',
    # columns=['patient_id', 'feature_id', 'feature_value'], index=False)
    return aggregated_events


def calc_event_info(values):
    if "LAB" in values["event_id"].values[0]:
        out = values["value"].count()
    elif "DIAG" in values["event_id"].values[0]:
        out = values["value"].values.sum()
    elif "DRUG" in values["event_id"].values[0]:
        out = values["value"].values.sum()
    else:
        out = None

    values["value"] = out
    return values


def normalize_event_info(values):
    feature_value = values["feature_value"].values
    max_val = float(feature_value.max())
    normalized_vals = (feature_value) / (max_val)
    values["feature_value"] = normalized_vals
    return values


def pivot_features(features_df):
    complete_events = features_df.loc[:, ["patient_id", "feature_id", "feature_value"]]

    pivoted_features = complete_events.pivot(index='patient_id', columns='feature_id', values='feature_value').fillna(
        0.0)

    return pivoted_features


def build_feature_dict(aggregated_events, mortality=pd.DataFrame()):
    if not mortality.empty:
        complete_events = aggregated_events.join(mortality.set_index("patient_id"), on="patient_id", how="outer")

        complete_events = complete_events.loc[:, ["patient_id", "feature_id", "feature_value", "label"]] \
            .dropna(subset=["feature_id", "feature_value"]).fillna(0.0)
    else:
        complete_events = aggregated_events.loc[:, ["patient_id", "feature_id", "feature_value"]]
        complete_events["label"] = 1.0

    patient_features = {}
    mortality = {}

    for index, row in complete_events.iterrows():
        patient_id = row["patient_id"]
        feature_id = row["feature_id"]
        if feature_id == 0:
            print patient_id, feature_id
        feature_value = row["feature_value"]
        if patient_id not in patient_features.keys():
            patient_features[patient_id] = [(feature_id, feature_value)]
        else:
            patient_features[patient_id].append((feature_id, feature_value))

        mortality[patient_id] = row['label']

    return patient_features, mortality


def to_svm_lite_train(patient_features, mortality, file_name):
    file = open(file_name, 'wb')

    patient_ids = patient_features.keys()
    patient_ids.sort()
    for id in patient_ids:
        features = patient_features[id]
        features = sorted(features, key=itemgetter(0))
        label = mortality[id]
        file.write("{0} {1} \n".format(int(label), utils.bag_to_svmlight(features)))


def to_svm_lite_test(patient_features, file_name):
    file = open(file_name, 'wb')

    patient_ids = patient_features.keys()
    patient_ids.sort()
    for id in patient_ids:
        features = patient_features[id]
        features = sorted(features, key=itemgetter(0))
        file.write("{0} {1} \n".format(int(id), utils.bag_to_svmlight(features)))


def my_features():
    train_events_df, train_mortality_df, train_feature_map_df = utils.read_csv("../data/train/")
    test_events_df, test_feature_map_df = read_test_csv("../data/test/")

    train_features = build_features(train_events_df, train_feature_map_df)
    train_features_dict, train_mortality_dict = build_feature_dict(train_features, train_mortality_df)

    to_svm_lite_train(train_features_dict, train_mortality_dict, "train_features.train")

    test_features = build_features(test_events_df, test_feature_map_df)
    test_features_dict, test_mortality_dict = build_feature_dict(test_features)

    to_svm_lite_test(test_features_dict, "../deliverables/test_features.txt")

    X_train, Y_train = utils.get_data_from_svmlight("train_features.train")
    X_test, Y_test = utils.get_data_from_svmlight("../deliverables/test_features.txt")

    return X_train, Y_train, X_test


def my_classifier_predictions(X_train, Y_train, X_test):
    elements = Y_train.size
    kfold = KFold(n=elements, n_folds=5)
    accuracy_list = []
    auc_list = []
    models = []
    for train_index, test_index in kfold:
        X_train_split, Y_train_split, X_test_split, Y_test_true = split_data(X_train, Y_train, train_index, test_index)
        clf_tree = GradientBoostingClassifier(n_estimators=40)
        clf_tree.fit(X_train_split, Y_train_split)
        test = X_test_split.todense()
        Y_test_pred = clf_tree.predict(test)
        accuracy, auc = calc_metrics(Y_test_true, Y_test_pred)
        accuracy_list.append(accuracy)
        auc_list.append(auc)
        models.append(clf_tree)

    print "avg accuracy: {}".format(mean(accuracy_list))
    print "avg auc: {}".format(mean(auc_list))

    test = X_test.todense()
    pred = None
    for tree in models:
        if pred is None:
            pred = tree.predict(test)
        else:
            pred += tree.predict(test)

    pred = np.divide(pred, len(models)).astype(int)
    return pred


def calc_metrics(Y_true, Y_pred):
    accuracy = metrics.accuracy_score(y_true=Y_true, y_pred=Y_pred)
    fpr, tpr, thresholds = metrics.roc_curve(y_true=Y_true, y_score=Y_pred)
    auc = metrics.auc(fpr, tpr)
    return accuracy, auc


def split_data(X, Y, train_index, test_index):
    X_train = X[train_index, :]
    Y_train = Y[train_index]
    X_test = X[test_index, :]
    Y_test_true = Y[test_index]
    return X_train, Y_train, X_test, Y_test_true


def classification_metrics(Y_pred, Y_true):
    accuracy = metrics.accuracy_score(y_true=Y_true, y_pred=Y_pred)
    fpr, tpr, thresholds = metrics.roc_curve(y_true=Y_true, y_score=Y_pred)
    auc = metrics.auc(fpr, tpr)
    precision = metrics.precision_score(y_true=Y_true, y_pred=Y_pred)
    recall = metrics.recall_score(y_true=Y_true, y_pred=Y_pred)
    f1score = metrics.f1_score(y_true=Y_true, y_pred=Y_pred)
    return accuracy, auc, precision, recall, f1score


def display_metrics(classifierName, Y_pred, Y_true):
    print "______________________________________________"
    print "Classifier: " + classifierName
    acc, auc_, precision, recall, f1score = classification_metrics(Y_pred, Y_true)
    print "Accuracy: " + str(acc)
    print "AUC: " + str(auc_)
    print "Precision: " + str(precision)
    print "Recall: " + str(recall)
    print "F1-score: " + str(f1score)
    print "______________________________________________"
    print ""


def main():
    X_train, Y_train, X_test = my_features()
    Y_train_pred = my_classifier_predictions(X_train, Y_train, X_train)
    # display_metrics("Training Predictions", Y_train_pred, Y_train)
    Y_pred = my_classifier_predictions(X_train, Y_train, X_test)
    utils.generate_submission("../deliverables/test_features.txt", Y_pred)


# The above function will generate a csv file of (patient_id,predicted label) and will be saved as "my_predictions.csv" in the deliverables folder.

if __name__ == "__main__":
    main()
