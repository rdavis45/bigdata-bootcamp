import models_partc
from sklearn.cross_validation import KFold, ShuffleSplit
from numpy import mean

import utils

# USE THE GIVEN FUNCTION NAME, DO NOT CHANGE IT

# USE THIS RANDOM STATE FOR ALL OF YOUR CROSS
# VALIDATION TESTS OR THE TESTS WILL NEVER PASS
RANDOM_STATE = 545510477


# input: training data and corresponding labels
# output: accuracy, auc
def get_acc_auc_kfold(X, Y, k=5):
    # TODO:First get the train indices and test indices for each iteration
    # Then train the classifier accordingly
    # Report the mean accuracy and mean auc of all the folds
    elements = Y.size
    kfold = KFold(n=elements, n_folds=k, random_state=RANDOM_STATE)
    accuracy_list = []
    auc_list = []
    for train_index, test_index in kfold:
        accuracy, auc = split_and_test(X, Y, train_index, test_index)
        accuracy_list.append(accuracy)
        auc_list.append(auc)
    mean_accuracy = mean(accuracy_list)
    mean_auc = mean(auc_list)
    return mean_accuracy, mean_auc


# input: training data and corresponding labels
# output: accuracy, auc
def get_acc_auc_randomisedCV(X, Y, iterNo=5, test_percent=0.2):
    # TODO: First get the train indices and test indices for each iteration
    # Then train the classifier accordingly
    # Report the mean accuracy and mean auc of all the iterations
    shfl = ShuffleSplit(n=Y.size, n_iter=iterNo, test_size=test_percent)
    accuracy_list = []
    auc_list = []
    for train_index, test_index in shfl:
        accuracy, auc = split_and_test(X, Y, train_index, test_index)
        accuracy_list.append(accuracy)
        auc_list.append(auc)

    mean_accuracy = mean(accuracy_list)
    mean_auc = mean(auc_list)
    return mean_accuracy, mean_auc

def split_and_test(X, Y, train_index, test_index):
    X_train, Y_train, X_test, Y_test_true = split_data(X, Y, train_index, test_index)
    Y_test_pred = models_partc.logistic_regression_pred(X_train, Y_train, X_test)
    accuracy, auc, precision, recall, f1score = models_partc.classification_metrics(Y_pred=Y_test_pred,
                                                                                    Y_true=Y_test_true)
    return accuracy, auc


def split_data(X, Y, train_index, test_index):
    X_train = X[train_index, :]
    Y_train = Y[train_index]
    X_test = X[test_index, :]
    Y_test_true = Y[test_index]
    return X_train, Y_train, X_test, Y_test_true


def main():
    X, Y = utils.get_data_from_svmlight("../deliverables/features_svmlight.train")
    print "Classifier: Logistic Regression__________"
    acc_k, auc_k = get_acc_auc_kfold(X, Y)
    print "Average Accuracy in KFold CV: " + str(acc_k)
    print "Average AUC in KFold CV: " + str(auc_k)
    acc_r, auc_r = get_acc_auc_randomisedCV(X, Y)
    print "Average Accuracy in Randomised CV: " + str(acc_r)
    print "Average AUC in Randomised CV: " + str(auc_r)


if __name__ == "__main__":
    main()
