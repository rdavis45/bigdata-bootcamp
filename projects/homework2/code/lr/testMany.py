#!/usr/bin/env python

"""
This file should test the model you develop in LogisticRegressionSGD,
it should not be changed
"""

import sys
import pickle
from optparse import OptionParser

from sklearn.metrics import roc_curve, auc
from sklearn.datasets import load_svmlight_file

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

from lrsgd import LogisticRegressionSGD
from utils import parse_svm_light_data

if __name__ == '__main__':

    learning_rates = [x / 100.0 for x in range(1, 11)]

    regularization = [x / 100.0 for x in range(1, 11)]

    heat = np.ones((len(learning_rates), len(regularization)))

    data = []
    for X, y in parse_svm_light_data(sys.stdin):
        data.append((X, y))

    for l in learning_rates:
        for r in regularization:
            lint = int(l * 100)
            rint = int(r * 100)

            inpath = "code/models/model_{0}_{1}.txt".format(lint, rint)
            outpath = "code/plots/model_{0}_{1}.png".format(lint, rint)

            with open(inpath, 'rb') as f:
                classifier = pickle.load(f)
                y_test_prob = []
                y_test = []
                for d in data:
                    y_prob = classifier.predict_prob(d[0])
                    y_test.append(d[1])
                    y_test_prob.append(y_prob)

                fpr, tpr, _ = roc_curve(y_test, y_test_prob)
                roc_auc = auc(fpr, tpr)

                heat[lint-1, rint-1] = roc_auc

                # Plot of a ROC curve for a specific class
                plt.figure()
                plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
                plt.plot([0, 1], [0, 1], 'k--')
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                plt.title('Receiver operating characteristic')
                plt.legend(loc="lower right")
                plt.savefig(outpath)
                plt.close()

    plt.figure()
    plt.imshow(heat, cmap='hot', interpolation='nearest')
    plt.savefig("code/plots/heat.png")