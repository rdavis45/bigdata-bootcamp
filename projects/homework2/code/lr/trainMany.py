
import sys
import pickle

from lrsgd import LogisticRegressionSGD
from utils import parse_svm_light_data

if __name__ == '__main__':
    learning_rates = [x / 100.0 for x in range(1, 11)]

    regularization = [x / 100.0 for x in range(1, 11)]

    n_features = 3618

    data = []
    for X, y in parse_svm_light_data(sys.stdin):
        data.append((X, y))

    for l in learning_rates:
        for r in regularization:
            print l
            print r
            classifier = LogisticRegressionSGD(l, r, n_features)
            for d in data:
                classifier.fit(d[0], d[1])

            path = "code/models/model_{0}_{1}.txt".format(int(l*100),int(r*100))

            with open(path, "wb") as f:
                pickle.dump(classifier, f)
            classifier = None